INTRODUCTION
------------

The Brave Browser enables users to tip websites they want to support with the BAT crypto currency.

The Brave module enables a Drupal site to verify itself as a content creator in order to receive BAT tips.

For more information on the Brave browser:

https://brave.com

For more information on registering as a content creator:

https://creators.brave.com

Once you're using the Brave browser and want to try out the tipping process and support this module: 

https://blockpositive.com



REQUIREMENTS
------------

This module requires no modules outside of Drupal core.



INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.



CONFIGURATION
-------------

 * Configure the user permissions in Administration » People » Permissions:

   - Manage Brave verification

     Set which role can enter Brave verification details.

*  Get Brave verification details for your site:

   https://creators.brave.com/sign-up
 
 * Set Brave verification details in Administration » Web services » Brave verify

 * Check the verification details are accessable at:
   
   [your domain]/.well-known/brave-rewards-verification.txt

 * Check on your Brave Rewards dasboard that your site has been verified:
   
   https://publishers.basicattentiontoken.org/publishers/home



MAINTAINERS
-----------

Current maintainers:

* Robert Castelo (robertcastelo) - https://www.drupal.org/u/robert-castelo