<?php

namespace Drupal\brave\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;


/**
 * Implements a form for Brave verification info.
 */
class VerifyForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'brave_verify_form';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['brave.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('brave.settings');

    $form['verification'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Verification Details'),
      '#default_value' => $config->get('verification'),
    ];

    $form['actions']['#type'] = 'actions';

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (strlen($form_state->getValue('verification')) < 3) {
        // @todo check details.
      $form_state->setErrorByName('verification', $this->t('The phone number is too short. Please enter a full phone number.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->config('brave.settings');
    $config->set('verification', $form_state->getValue('verification'));
    $config->save();

    $link = Link::createFromRoute('Brave verification page', 'brave.verify');
    $this->messenger()->addStatus($this->t('@link now active.', ['@link' => $link->toString()]));
    
    parent::submitForm($form, $form_state);
  }

}
