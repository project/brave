<?php

namespace Drupal\brave\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * An example controller.
 */
class VerifyController extends ControllerBase {

  public function content() {

    $verification_info = \Drupal::config('brave.settings')->get('verification');

    $response = new Response();
    $response->setContent($verification_info);
    $response->headers->set('Content-Type', 'text/plain');
    $response->headers->set('Cache-Control', 'max-age=600,public');
    return $response;
  }

}



